all: out/presentation.html

resources = $(wildcard *.jpg) s5

out/presentation.html: presentation.md Makefile $(wildcard s5/*)
	@mkdir -p $(dir $@)
	@pandoc --self-contained --standalone -f markdown -t s5 -o $@ $<
