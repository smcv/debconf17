% A Debian maintainer's guide to Flatpak
% Simon McVittie, `smcv@{collabora.com,debian.org}`
% DebConf 17, Montreal

# Introduction to Flatpak

A sandboxed app framework for Linux (formerly xdg-app)

. . .

* app
    * as in `/usr/share/applications`
    * … or as in “app store”

. . .

* sandboxed
    * you trust them enough to use them
    * but not necessarily enough to give them access to `~/.gnupg`

. . .

* Linux
    * no attempt to be portable away from GNU/Linux
    * systemd helps but is not currently required
    * not specific to Debian, Fedora, Ubuntu …

# Apps and runtimes

![Fig.1. A stable flat-pack-based platform](helping.jpg){ width=480px height=360px }

# Linux desktops: a moving target

* ISVs can't target “generic Linux” like they can target Windows,
  or OS X, or iOS, or Android

* Traditional answer: pick a baseline version from the distant past
    * Assume the most common libraries are installed
    * Bundle the rest (static linking or `-rpath` or `LD_LIBRARY_PATH`)

. . .

* The LSB defines a standard baseline version
    * but it seems hardly any ISVs use it in practice
    * go to Didier Raboud's talk on Saturday!

# Bad for ISVs, and bad for us

* When we add a feature, ISVs don't get to use it for several years
    * Ensures they have “the platform problem”

. . .

* When ISVs do the right thing and help us add features/fix bugs,
  they **still** don't get to rely on that for several years
    * Disincentive to do the right thing

. . .

* The ISV is responsible for keeping up with security updates
    * Spoilers: sometimes they don't

# Backports are a pain, too

* Some software moves (much) faster than Debian's release cycle

> * `-backports` works, to a point, but…
>       * backports have be in testing (suitable for the next release)
>       * dependencies have to be in stable (old GTK)
>       * or, dependencies have to be backported, and destabilise
>         the platform layer (backport GTK and all of GNOME gets it)

# Flatpak runtimes

* Basically a small `/usr` (with the `/usr` merge)
    * Anyone can publish a runtime
    * Set of libraries chosen by the runtime maintainer
    * Versions chosen by the runtime maintainer
    * Flatpak app doesn't see the real `/usr`
    * No development tools
    * No package manager
    * No init/boot/sysadmin layer

. . .

* Reference runtimes: `org.freedesktop.Platform`, `org.gnome.Platform`

* KDE and Fedora also produce runtimes

. . .

* soon: Valve/Steam? Debian?

# Flatpak runtimes

* Demo 1: Minetest

# Flatpak SDKs

* A larger `/usr`, with developer tools
    * compiler
    * header files
    * static libraries
    * strace

* `com.example.Sdk` conventionally goes with `com.example.Platform`

# Flatpak runtime versioning

* Each runtime has *branches*

* An app depends on a specific branch
    * `org.gnome.gedit//stable` depends on `org.gnome.Platform//3.24`

. . .

* Examples
    * `org.gnome.Platform//3.24`
    * `org.fedoraproject.Platform//26`
    * `net.debian.flatpak.Games.Platform//stretch`

# The long tail

* Not everything can be available in a runtime
    * unless we really want that?

. . .

* The app can bundle extras
    * Uncommon libraries
    * Libraries where the latest version is needed

# App appears in `/app`

* Canonically, you recompile with `--prefix=/app`

* Do the same for bundled libraries

* The app does *not* need to be fully relocatable
    * Hard-coding `/app` in binaries is completely fine
    * Never relocated to `/home/jsmith/local` or similar

. . .

* `/app` is the same length as `/usr`
    * As a last resort, this is good for binary editing

# How apps and runtimes work

* `libostree`: like git, but for `/usr`

    ```
    ./config
    ./refs/heads/master
    ./refs/heads/stable
    ./refs/remotes/origin/master
    ./objects/3b/6f018809252480b740a48ea3cb746a434dd688
    ./objects/c0/837b81795498042a3570b792cb2f41da0a0551
    ```

. . .

* No, wait, that was git

# How apps and runtimes work

* `libostree`: like git, but for `/usr`

    ```
    ./config
    ./refs/heads/app/org.debian.packages.openarena/x86_64/master
    ./refs/remotes/flatdeb/runtime/net.debian.flatpak.Games.Platform/x86_64/stretch
    ./objects/a1/443a265b155be7d190c5a0a5e99427716a0a1432f6994fbde40aafb23fb11e.file
    ./objects/e9/67eda76106efa124b736af20c14b3fea2a254b71927534bd869217e105c532.file
    ```

# How apps and runtimes work

* Mount namespace
    * Mounts get isolated from the host system
    * The new root directory is a tmpfs

* Mount `${runtime}/files` on `/usr`
    * that's why we need to do the `/usr` merge here

* Mount `${app}/files` on `/app`

# Sandboxing with bubblewrap

![Fig.2. Protecting an important subject with bubble wrap](bubblewrap.jpg){ width=480px height=360px }

# Why sandbox

* I trust this app's author enough to run it

. . .

* … but not enough to let it control the rest of my system

. . .

* … but they might have made an exploitable mistake

. . .

* … but a bundled library might be vulnerable

# Namespaces

* Recent Linux kernels offer lots of namespaces
    * user (uid)
    * process (pid)
    * mount table
    * network
    * IPC (SysV, etc.)
    * UTS (uname)

* Best case: we can just use them
    * *unprivileged user namespaces*

# Namespaces

* User namespaces are scary
    * Unprivileged user gets capabilities in the userns
    * New improved Linux, now with extra attack surface!
    * e.g. CVE-2016-3135

. . .

* Upstream Linux: on or off at compile time, no middle ground
    * Ubuntu: enabled

. . .

* Debian: no unprivileged userns by default, adjust a sysctl at
  runtime to enable

. . .

* RHEL 7: no unprivileged userns by default, adjust a parameter at
  boot time to enable

# Bubblewrap (`bwrap`)

* Small setuid helper
    * formerly Flatpak's `xdg-app-helper`
    * descended from `linux-user-chroot`

* Small subset of userns

. . .

* Enough for Flatpak (and Red Hat's Project Atomic, and hopefully more)

. . .

* Not enough to be a huge attack surface
    * Doesn't execute user code without `PR_SET_NO_NEW_PRIVS` first

. . .

* Design principle: Does not allow anything that would be
  unreasonable for the kernel to allow

# Sandboxing

* Declarative capabilities
    * “uses X11”
    * “reads and writes `~/Downloads`”

* Same general idea as Android permissions

. . .

* Example

    ```
    [Context]
    shared=network;ipc;
    sockets=x11;wayland;pulseaudio;
    devices=dri;
    ```

# Thinking with portals

![Fig.3. Portals have safety implications if used carelessly](thinking-with-portals-cropped.jpg){ width=480px height=360px }

# Thinking with portals

* Better than handing out permissions: get user consent when needed

. . .

* When done badly, you get browser SSL warnings
    * Do you want to allow ghkscjjtklgoghkjxhtht? `[yes]` `[no]`

. . .

* When done well, it's fairly obvious what's going on
    * Let hangouts.google.com use your microphone? `[yes]` `[no]`

# Document portal

* Demo 2: GNOME Recipes

# Document portal

* Looks like a normal dialog box

* Implicitly gets permission

* Selected file magically appears in a FUSE filesystem in the sandbox

. . .

* Library support: recent GLib transparently uses portals

# How portals work

* Filtered access to host system's D-Bus session bus
    * Currently through a proxy
    * smcv is working on enhancing `dbus-daemon` to do this itself

. . .

* The filter always allows talking to `org.freedesktop.portal.*`
    * Reference (and only) implementation: `xdg-desktop-portal`

. . .

* `xdg-desktop-portal` talks to a per-desktop implementation
    * GNOME, KDE, more?

# Achieving world domination

![Fig.4. IKEA would put the entire world in their warehouse if they could](aisle-cropped.jpg){ width=480px height=360px }

# Not everything is an “app”

… and that's OK

. . .

* Distros provide the platform for the system
    * init
    * udev
    * NetworkManager
    * gdm (and authentication in general)
    * Flatpak
    * Portals

. . .

* Distros provide the platform for each user
    * GNOME Shell can't be an app
    * Neither can gnome-settings-daemon

# Not everything is an “app”

* CLIs and development tools

. . .

* Servers and daemons in general
    * Flatpak is for desktop apps
    * Put your servers in VMs, or Docker, or similar

# Curation and QA

> Every package has a maintainer who is often an expert in the field of
> the package
>
> — Raphaël Hertzog,
> [*State of the Debian-Ubuntu relationship*](https://raphaelhertzog.com/2010/12/06/state-of-the-debian-ubuntu-relationship/)

* At our best, we're domain experts on leaf packages (apps)

* Debian is a ready-made source of things that ought to be ready to be
  high-quality Flatpak apps

* We fix their bugs even if upstreams don't

. . .

* We fix their security flaws even if upstreams don't

# Building runtimes

* Distributions are good at providing libraries

* Debian takes libraries very seriously

* Debian takes stability very seriously

* Debian takes security very seriously

* These are just what you want in a runtime

# Distributions aren't perfect

* At our worst, we're just getting in the way

. . .

* What value do we add to `gnome-mines`?
    * Most uploads are just “New upstream release”

> * What value do we add to `xonotic`?
>     * Trick question, it has been ITP since 2011

# This doesn't need to be either/or

* Apps with good upstream maintenance can come from upstream

* Apps where we are genuinely helping can come from Debian

* We get some of the advantages either way

# Some assembly required

![Fig.5. A flat-pack stack ready for compilation](flatpack.jpg){ width=480 height=360 }

# Using Debian for runtimes

* Demo 3: OpenArena

# Using Debian for runtimes

[Flatdeb](https://git.collabora.com/cgit/user/smcv/flatdeb.git/):

* A prototype of building Flatpak runtimes with `apt`/`dpkg`

* Also a prototype of building Flatpak apps with `apt`/`dpkg`

. . .

* Simple case: the app is already relocatable

. . .

* Less simple case: the app needs build-time changes
    * or source changes

# Alternatives

![Fig.6. Flat-pack technology is not suitable for all use cases](throne.jpg){ width=480 height=360 }

# `apt`

* System-wide, root-privileged, non-atomic

* No sandboxing unless you specifically add it

* Good for: system services, the platform layer of desktop environments

* Good for: when upgrades are a big deal anyway
    * Debian stable
    * Ubuntu LTS

* Bad for: third-party software

# `libostree`-based deployment

* Same packages as `apt`, but different trade-offs

* Atomic, whole-tree deployment
    * … but it does need a reboot

* Good for: appliance-style systems with known functionality

* Bad for: third-party software

* Good for: combining with Flatpak
    * hi Endless!

# System-level containers

* Docker, systemd-nspawn and friends

* Good for: servers without extensive host OS requirements
    * or no host OS at all — “application containers”

* Good for: development and debugging

* Bad for: stopping container-root becoming real root

* Bad for: desktop integration

# Virtualization

* An entire other OS, including the kernel

* Good for: servers, “appliances”

* Good for: code you want to keep at arm's length

* Bad for: maximal efficiency

* Bad for: desktop integration

# Snap

* Relocatable sandboxed apps
    * Not just desktop, not just unprivileged
    * Libraries bundled (can be shared) or assumed present

* Application code needs to be relocatable

* Sandboxed (sometimes)

* Good for: Ubuntu kernels

* Bad for: mainstream kernels

# AppImage

* App is a wrapped filesystem image that can be made
  executable and run

* Dependencies must be either bundled or assumed

* Good for: Windows- and Mac-like UX - just download and run

* Bad for: getting updates

* Bad for: avoiding the moving target problem

* Bad for: deduplication

# Firejail

* Setuid program to set up sandboxes, like bubblewrap

* Uses the same technologies as bubblewrap

* Much more general and configurable than bubblewrap
    * Much more attack surface than bubblewrap

* Good for: AppImage integration

* Good for: users who are already root-equivalent anyway

* Bad for: users who should not become root-equivalent

# Questions?

Slides, source code and flatdeb: <https://flatpak.debian.net/>

<div class="fine-print">

* flatdeb: <https://git.collabora.com/cgit/user/smcv/flatdeb.git/>
* Slides and demos: <https://git.collabora.com/cgit/user/smcv/dc17.git/>

All content except images © 2017 Collabora Ltd.
[CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/)

Image credits:

* Stable platform cat:
  [Simon_sees](https://www.flickr.com/photos/39551170@N02/11618280895/), 2013.
  [CC-BY-2.0](https://creativecommons.org/licenses/by/2.0/)
* Bubblewrap cat:
  [donabelandewen](https://www.flickr.com/photos/donabelandewen/4106414108/), 2009.
  [CC-BY-2.0](https://creativecommons.org/licenses/by/2.0/)
* Thinking with portals:
  [roninkengo](https://www.flickr.com/photos/roninkengo/2321788223/in/photostream/), 2008.
  [CC-BY-2.0](https://creativecommons.org/licenses/by/2.0/)
* Warehouse aisle:
  [themightycondorman](https://www.flickr.com/photos/themightycondorman/6720879311/), 2012.
  [CC-BY-2.0](https://creativecommons.org/licenses/by/2.0/)
* Some assembly required:
  [alberth2](https://www.flickr.com/photos/alberth2/19093719026/in/photolist-v6fisb-uR7tDX-878Yh5-v6fniN-ubHzci-51CjZX-brVGNV-r77tZK-79kpCS-oaR8oC-nmPXEd-RUVXYh-3ca9xV-8dG44V-6EuWok-evngDb-6Qd6fw-wYuDo-8aBNwZ-82iqYW-a115Bv-6Qd5YW-q48HGH-anWQsD-qkvJo5-qkz4ZX-q4acf4-afLway-82t5Zf-4GBTR9-m8sP2K-bNBbvT-7hHJLH-8aCq7v-59T7s3-e8KL4Q-hPPGph-SnLUev-ScbX65-5tMsWL-dZnXug-5d4mhZ-iXKqp4-ubxVPS-6hqegS-5nH5Dj-ByEX4w-5nH55J-6xHCUx-6Qd7Wu), 2015.
  [CC-BY-SA-2.0](https://creativecommons.org/licenses/by-sa/2.0/)
* Throne:
  [Kinshuk Sunil](https://www.flickr.com/photos/kinshuksunil/3582226486/), 2009.
  [CC-BY-2.0](https://creativecommons.org/licenses/by/2.0/)

</div>
